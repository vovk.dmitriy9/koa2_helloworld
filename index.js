const Koa = require('koa');
const app = new Koa();
const router = require('@koa/router')();
const mount = require('koa-mount');
const serve = require('koa-static');

router
  .get('/', (ctx, next) => {
    ctx.body = { message: 'hello world' };
  })
  .get('/ping', (ctx, next) => {
    ctx.body = { message: 'pong' };
  });

const chatapp = new Koa();
app.use(serve('./static'));
chatapp.use(mount('/static', app));

app.use(router.routes()).use(router.allowedMethods());

chatapp.listen(3000);
